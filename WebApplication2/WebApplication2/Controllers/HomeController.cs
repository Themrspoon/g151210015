﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.DAL;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {

        private siteContext db = new siteContext();
        public ActionResult Index(int? page)
        {
            var urunler = from s in db.Urunler
                          select s;

            int pageSize = 2;
            int pageNumber = (page ?? 1);

            return View(urunler.OrderBy(x => x.Fiyat).ToPagedList(pageNumber, pageSize));
        }
        [ChildActionOnly]
        public ActionResult _Slider()
        {
            return View();
        }
    }
}