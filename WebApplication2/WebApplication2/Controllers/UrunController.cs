﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.DAL;
using WebApplication2.Models;
using PagedList;
using System.IO;

namespace WebApplication2.Controllers
{
    public class UrunController : Controller
    {
        private siteContext db = new siteContext();


        // GET: Urun
        public ActionResult Index(int? page)
        {
            var urunler = from s in db.Urunler
                          select s;

            int pageSize = 2;
            int pageNumber = (page ?? 1);

            return View(urunler.OrderBy(x => x.Fiyat).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult SatinAl(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun urun = db.Urunler.Find(id);
            if (urun == null)
            {
                return HttpNotFound();
            }
            return View(urun);
        }

        public ActionResult Sirala(string currentFilter, int? page)
        {
            var urunler = from s in db.Urunler
                          select s;

            int pageSize = 9;
            int pageNumber = (page ?? 1);

            return View(urunler.OrderBy(x => x.Ad).ToPagedList(pageNumber, pageSize));



        }

        // GET: Urun/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun urun = db.Urunler.Find(id);
            if (urun == null)
            {
                return HttpNotFound();
            }
            return View(urun);
        }


        public ActionResult Search(string arama,int? page, string currentFilter)
        {

            if (arama != null)
            {
                page = 1;
            }
            else { arama = currentFilter; }
            ViewBag.CurrentFilter = arama;
            
            var urunler = from s in db.Urunler
                         select s;

            if (!String.IsNullOrEmpty(arama))
            {
                urunler = urunler.Where(s => s.Ad.ToUpper().Contains(arama.ToUpper())
                 ||
                s.Ad.ToUpper().Contains(arama.ToUpper()));
            }

            int pageSize = 9;
            int pageNumber = (page ?? 1);

            return View(urunler.OrderBy(x => x.Ad).ToPagedList(pageNumber, pageSize));

        }

        // GET: Urun/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Urun/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Urun urun, HttpPostedFileBase file)
        {
            Urun s = new Urun();
            s.Ad = urun.Ad;
            s.Fiyat = urun.Fiyat;
            s.Renk = urun.Renk;
            s.Beden = urun.Beden;
            s.Yildiz = urun.Yildiz;
            s.Tur = urun.Tur;
            s.Tarz = urun.Tarz;
            s.Baski = urun.Baski;
            if (file != null && file.ContentLength > 0)
            {
                MemoryStream memoryStream = file.InputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    file.InputStream.CopyTo(memoryStream);
                }
                s.UrunFoto = memoryStream.ToArray();
            }
            db.Urunler.Add(s);
            db.SaveChanges();
            return RedirectToAction("Index", "Urun");
        }

        public ActionResult Design()
        {
            return View();
        }

        // POST: Urun/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Design(Urun urun, HttpPostedFileBase file)
        {
            Urun s = new Urun();
            s.Ad = urun.Ad;
            s.Fiyat = urun.Fiyat;
            s.Renk = urun.Renk;
            s.Beden = urun.Beden;
            s.Yildiz = urun.Yildiz;
            s.Tur = urun.Tur;
            s.Tarz = urun.Tarz;
            s.Baski = urun.Baski;
            if (file != null && file.ContentLength > 0)
            {
                MemoryStream memoryStream = file.InputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    file.InputStream.CopyTo(memoryStream);
                }
                s.UrunFoto = memoryStream.ToArray();
            }
            db.Urunler.Add(s);
            db.SaveChanges();
            return RedirectToAction("Index", "Urun");
        }





        // GET: Urun/Edit/5
        public ActionResult Edit(int? id)
        {
            

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Urun urun = db.Urunler.Find(id);
                if (urun == null)
                {
                    return HttpNotFound();
                }
            return View(urun);





         
           
        }

        // POST: Urun/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Urun gUrun, HttpPostedFileBase file)
        {
            if (gUrun == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var s = db.Urunler.Where(x => x.UrunID == gUrun.UrunID).FirstOrDefault();
            if (file != null && file.ContentLength > 0)
            {
                MemoryStream memoryStream = file.InputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    file.InputStream.CopyTo(memoryStream);
                }
                s.UrunFoto = memoryStream.ToArray();
            }
            s.Ad = gUrun.Ad;
            s.Fiyat = gUrun.Fiyat;
            s.Renk = gUrun.Renk;
            s.Beden = gUrun.Beden;
            s.Yildiz = gUrun.Yildiz;
            s.Tur = gUrun.Tur;
            s.Tarz = gUrun.Tarz;
            s.Baski = gUrun.Baski;
            db.SaveChanges();
            return RedirectToAction("Index", "Urun");
        }



        // GET: Urun/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun urun = db.Urunler.Find(id);
            if (urun == null)
            {
                return HttpNotFound();
            }
            return View(urun);
        }

        // POST: Urun/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Urun urun = db.Urunler.Find(id);
            db.Urunler.Remove(urun);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
