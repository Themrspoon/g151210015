﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.DAL;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class DesignController : Controller

    {

        private siteContext db = new siteContext();
        // GET: Design
        // GET: Urun/Create
        public ActionResult Design()
        {
            return View();
        }

        // POST: Urun/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tasarim urun, HttpPostedFileBase file)
        {
            Tasarim s = new Tasarim();
            s.Ad = urun.Ad;
            s.Fiyat = urun.Fiyat;
            s.Renk = urun.Renk;
            s.Beden = urun.Beden;
            s.Yildiz = urun.Yildiz;
            s.Tur = urun.Tur;
            s.Tarz = urun.Tarz;
            s.Baski = urun.Baski;
            s.Aciklama = urun.Aciklama;
            s.Adres = urun.Adres;
            s.Email = urun.Email;
            s.Isim = urun.Isim;
            s.Tel = urun.Tel;
            if (file != null && file.ContentLength > 0)
            {
                MemoryStream memoryStream = file.InputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    file.InputStream.CopyTo(memoryStream);
                }
                s.UrunFoto = memoryStream.ToArray();
            }
            db.Urunler.Add(s);
            db.SaveChanges();
            return RedirectToAction("Design", "Design");
        }
    }
}