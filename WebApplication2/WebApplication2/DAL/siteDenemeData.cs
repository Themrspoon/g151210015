﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.Models;

namespace WebApplication2.DAL
{
    public class siteDenemeData:System.Data.Entity.DropCreateDatabaseIfModelChanges<siteContext>
    {
        protected override void Seed(siteContext context)
        {
            var Urunler = new List<Urun>
            {
      new Urun {Ad="Half Life Tişörtü", Fiyat=34, Renk=Renk.Siyah, Beden=Beden.Slimfit ,Yildiz=Reyting.Beş, Baski=Baski.Press, Tarz=Tarz.Oyun, Tur=Tur.Kumaş }
            };
            Urunler.ForEach(s => context.Urunler.Add(s));
            context.SaveChanges();

            var Tasarim = new List<Tasarim>
            {
                new Tasarim {Ad="Vaas Tişörtü", Fiyat=99, Renk=Renk.Beyaz, Beden=Beden.Large ,Yildiz=Reyting.Üç, Baski=Baski.Dijital, Tarz=Tarz.Dizi, Tur=Tur.İpek}
            };
            Tasarim.ForEach(s => context.Tasarimlar.Add(s));
            context.SaveChanges();

        }
    }
}