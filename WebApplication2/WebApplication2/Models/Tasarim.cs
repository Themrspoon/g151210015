﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Tasarim:Urun
    {
        public int TasarimID { get; set; }

        public string Isim { get; set; }

        public int Tel { get; set; }

        public string Adres { get; set; }

        public string Email { get; set; }

        public string Aciklama { get; set; }

       
    }                                                                                                   
}