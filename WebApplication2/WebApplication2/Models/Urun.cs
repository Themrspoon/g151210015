﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{

    public enum Reyting
    {
        Bir, İki, Üç, Dört, Beş

    }

    public enum Renk
    {
        Siyah, Beyaz, Mavi, Yeşil, Turuncu, Sarı, Kırmızı

    }

    public enum Beden
    {
        XtraSmall, Slimfit, Small, Medium, Large, XtraLarge

    }

    public enum Tur
    {
        Kumaş, İpek, Yün, Keten

    }

    public enum Tarz
    {
        Dizi, Film, Oyun, Tasarım

    }

    public enum Baski
    {
        Dijital, Press

    }

    public class Urun
    {
        public int UrunID { get; set; }

        public string Ad { get; set; }

        public int Fiyat { get; set; }

        public Renk Renk { get; set; }

        public Beden Beden { get; set; }

        public Reyting? Yildiz { get; set; }

        public Tur Tur { get; set; }

        public Tarz Tarz { get; set; }

        public Baski Baski { get; set; }

        public byte[] UrunFoto { get; set; }

    }
}